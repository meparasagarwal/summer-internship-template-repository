## Team Hack O' Holics - Image Store

Team Hack'o'Holics
Project - Image Auto Optimization Engine.

### To run the application
1. Open a terminal/command prompt window
2. `cd Application Code/Hack2Hirebackend`
3. `flask run`
4. Open another terminal/command prompt window
5. `cd Application Code/Hack2HireFrontend/client`
6. `npm start`

Note: Currently s3 is not activated, so the database part in the backend doesn't work.

### Project Overview
----------------------------------

We have built a user friendly web app where users can store the images in the compressed format and can retrieve the decompressed format of the images.

### Solution Description
To tackle the problem we made a web app where user can upload images and resize them. These uploaded images will get resized and compressed in the backend and the compressed file gets stored in the s3 database.
User can also download these images from our app, where the decompression takes place and user gets his original file

#### Architecture Diagram

Site Structure
![alt text](https://gitlab.com/meparasagarwal/summer-internship-template-repository/-/raw/master/Images/Site-Structure.jpg)

Database Structure
![alt text](https://gitlab.com/meparasagarwal/summer-internship-template-repository/-/raw/master/Images/database.jpg)


#### Technical Description
Image Resizing:
We have used Bicubic interpolation for resizing of the image.
The module need to run are:
->os 

Image Compression/Decompression
we have used a Generative model that is Autoencoders,It encodes the image into a 1D vector and then uses that 1D vector to reconstruct the image.

The modules need to run are:
->Pytorch
->pytorch vision
->tqdm
->PIL
->numpy
->lzma
To run the code -> flask run
### Team Members
----------------------------------

List of team member names and email IDs with their contributions

Ayush Raj: b518015@iiit-bh.ac.in,Contribution-Project Concept,Image optimisation,Compression and Decompression,Scripting.

Ritvik Nimmagadda: b118048@iiit-bh.ac.in, Contribution: Flask backend routes, Integration of Flask with React frontend, Resizing image, Thumbnail creation for Image gallery, Email confirmation for registration and forgot password.

Chandra Venkata Preethi: b118022@iiit-bh.ac.in, Contribution: UI Design, Front-End Development, React frontend Routing, Drag and drop feature for files, Resizing of the Image at the Client Side.

Paras Agarwal: b418030@iiit-bh.ac.in,Contribution:Configuring and Applying S3 API, Database(Saving user data and mapping filename),Resizing the image ,Flask Backend,Deployment,Project Design
