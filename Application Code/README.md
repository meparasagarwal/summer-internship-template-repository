## To run the application
1. Open a terminal/command prompt window
2. `cd Hack2Hirebackend`
3. `flask run`
4. Open another terminal/command prompt window
5. `cd Hack2HireFrontend/client`
6. `npm start`

Note: Currently s3 is not activated, so the database part in the backend doesn't work.
